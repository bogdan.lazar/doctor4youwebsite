const product_card = [
    {
        id: 1,
        question: "Should I worry about my cholesterol?",
        answer: "Cholesterol is a necessary fat substance that helps our bodies build cells and make hormones. However, excess cholesterol can clog arteries and lead to heart disease."
    },
    {
        id: 2,
        question: "What can I do to prevent cancer?",
        answer: "There's no certain treatment to prevent cancer, but there are habits that increase your risk of cancer. Avoiding habits like smoking, which contains harmful chemicals, can reduce your risk for cancer."
    },
    {
        id: 3,
        question: "Why can't I sleep?",
        answer: "The most common reasons people can’t sleep are caffeine, nicotine, or certain medications. Eliminating caffeinated drinks after noon can be beneficial toward your sleep routine."
    },
    {
        id: 4,
        question: "Should I see my provider for a cold?",
        answer: "Viruses cause colds, and there’s no treatment for viruses. However, there are over-the-counter medicines, like decongestants, that can provide relief from symptoms."
    }
]
export default product_card;